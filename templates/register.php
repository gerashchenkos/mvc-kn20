<div class="container-fluid">
    <div class="col-md-4">
        <form method="POST" action="/register.php">
            <div class="form-outline mb-4">
                <label class="form-label" for="firstName">First Name</label>
                <input type="text" id="firstName" name="firstName" class="form-control <?php if(!empty($error['firstName'])) echo 'is-invalid';?>"
                    value="<?php echo ($_POST['firstName'] ?? '');?>"/>
                <div class="invalid-feedback">
                    <?php echo $error['firstName'] ?? '';?>
                </div>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label" for="lastName">Last Name</label>
                <input type="text" id="lastName" name="lastName" class="form-control <?php if(!empty($error['lastName'])) echo 'is-invalid';?>"
                value="<?php echo ($_POST['lastName'] ?? '');?> "/>
                <div class="invalid-feedback">
                    <?php echo $error['lastName'] ?? '';?>
                </div>
            </div>
            <!-- Email input -->
            <div class="form-outline mb-4">
                <label class="form-label" for="form2Example1">Email address</label>
                <input type="email" id="form2Example1" name="email" class="form-control <?php if(!empty($error['email'])) echo 'is-invalid';?>"/>
                <div class="invalid-feedback">
                    <?php echo $error['email'] ?? '';?>
                </div>
            </div>

            <!-- Password input -->
            <div class="form-outline mb-4">
                <label class="form-label" for="form2Example2">Password</label>
                <input type="password" name="password" id="form2Example2" class="form-control <?php if(!empty($error['password'])) echo 'is-invalid';?>"/>
                <div class="invalid-feedback">
                    <?php echo $error['password'] ?? '';?>
                </div>
            </div>

            <!-- Password input -->
            <div class="form-outline mb-4">
                <label class="form-label" for="confirmPassword">Confirm Password</label>
                <input type="password" name="confirmPassword" id="confirmPassword" class="form-control <?php if(!empty($error['confirmPassword'])) echo 'is-invalid';?>"/>
                <div class="invalid-feedback">
                    <?php echo $error['confirmPassword'] ?? '';?>
                </div>
            </div>

            <div class="form-outline mb-4">
                <select name="country" class="form-select <?php if(!empty($error['country'])) echo 'is-invalid';?>" id="country" required>
                    <option value="">Select country</option>
                    <?php
                    foreach ($countries as $k => $v): ?>
                        <option value="<?php
                        echo $k; ?>"><?php
                            echo $v; ?></option>
                    <?php
                    endforeach; ?>
                </select>
                <div class="invalid-feedback">
                    <?php echo $error['country'] ?? '';?>
                </div>
            </div>

            <!-- Submit button -->
            <input type="submit" class="btn btn-primary btn-block mb-4" value="Sign up">
    </div>
    </form>
</div>
</div>
