<div class="container-fluid">
    <a href="/new.html" target="_blank">New Page</a>
    <div class="row">
        <div class="col-md-4">
            <h2>Hello, <?php echo $_SESSION['user']; ?></h2>
        </div>
        <div class="col-md-4">
            <form action="" method="post" class="center">
                <div class="form-group mb-3">
                    <label class="form-label" for="id">Id:</label>
                    <input type="number" min="1" max="1000000" class="form-control" name="id" id="id" required>
                </div>
                <div class="form-group mb-3">
                    <label class="form-label" for="name">Name:</label>
                    <input type="text" name="name" class="form-control" id="name" required>
                </div>
                <div class="form-group mb-4">
                    <label class="form-label" for="description">Description:</label>
                    <textarea name="description" class="form-control" id="description" required></textarea>
                </div>
                <div class="form-group input-group mb-4">
                    <span class="input-group-text">Price</span>
                    <span class="input-group-text">0.00</span>
                    <input type="number" min="1.00" max="10000.00" step="0.01" class="form-control" name="price"
                           id="price" required/>
                </div>
                <div class="form-group input-group mb-4">
                    <span class="input-group-text">Quantity</span>
                    <input type="number" min="1" max="1000" step="1" class="form-control" name="quantity" id="quantity"
                           required/>
                </div>
                <div class="form-group mb-3">
                    <label class="form-label" for="category">Category:</label>
                    <select name="category" class="form-select" id="category" required>
                        <option value="">Select category</option>
                        <option value="phones">Phones</option>
                        <option value="tv">TV</option>
                        <option value="books">Books</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <input type="submit" class="btn btn-primary" value="Add Product">
                </div>
            </form>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
