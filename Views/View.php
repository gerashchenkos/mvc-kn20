<?php

namespace MVC\Views;

class View
{
    private const TEMPLATES_PATH = ROOT_PATH . "templates";
    private const TEMPLATES_PARTIALS_PATH = ROOT_PATH . "templates" . DIRECTORY_SEPARATOR . "partials";
    private const TEMPLATES_EXT = ".php";
    private bool $enablePartials;

    public function __construct(bool $flag = true)
    {
        $this->setEnablePartials($flag);
    }

    public function render(string $template, array $data = [])
    {
        extract($data);
        if ($this->enablePartials) {
            require_once self::TEMPLATES_PARTIALS_PATH . DIRECTORY_SEPARATOR . "header" . self::TEMPLATES_EXT;
        }
        require_once self::TEMPLATES_PATH . DIRECTORY_SEPARATOR . $template . self::TEMPLATES_EXT;
        if ($this->enablePartials) {
            require_once self::TEMPLATES_PARTIALS_PATH . DIRECTORY_SEPARATOR . "footer" . self::TEMPLATES_EXT;
        }
    }

    public function setEnablePartials(bool $flag)
    {
        $this->enablePartials = $flag;
    }
}