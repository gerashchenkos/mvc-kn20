<?php

define("ROOT_PATH", dirname(__FILE__, 2) . DIRECTORY_SEPARATOR);

require_once ROOT_PATH . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();

use MVC\Routes\Route;

$dotenv = Dotenv\Dotenv::createImmutable(ROOT_PATH);
$dotenv->load();
Route::setRoutes(
    [
        ["url" => "/", "controller" => "Product@index"],
        ["url" => "/user/register", "controller" => "User@register"],
        ["url" => "/user/login", "controller" => "User@login"],
    ]
);
Route::init($_SERVER['REQUEST_URI']);

