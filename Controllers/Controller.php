<?php


namespace MVC\Controllers;

use MVC\Views\View;

class Controller
{
    public $view;

    public function __construct()
    {
        $this->view = new View();
    }
}