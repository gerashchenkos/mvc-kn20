<?php

namespace MVC\Controllers;
use MVC\Views\View;
use MVC\Models\User as UserModel;

class User extends Controller
{
    private const COUNTRIES = [
        "ua" => "Ukraine",
        "uk" => "Great Britain",
        "pl" => "Poland"
    ];

    public function login()
    {
        if (!empty($_POST['email']) && !empty($_POST['password'])) {
            $user = UserModel::login($_POST['email'], $_POST['password']);
            if ($user) {
                $_SESSION['user'] = $user->email;
                if (!empty($_POST['remember_me'])) {
                    setcookie("user",  $user->email, time()+3600 * 24);
                }
                header("Location: /");
                die();
            }
        }
        $this->view->render('login');
    }

    public function register()
    {
        $this->view->render('register', [
            "countries" => self::COUNTRIES
        ]);
    }
}