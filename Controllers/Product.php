<?php

namespace MVC\Controllers;
use MVC\Views\View;

class Product extends Controller
{
    public function index()
    {
        if (empty($_SESSION['user'])) {
            if (!empty($_COOKIE['user'])) {
                $_SESSION['user'] = $_COOKIE['user'];
            } else {
                header("Location: /user/login");
                die();
            }
        }
        $this->view->render('index');
    }
}