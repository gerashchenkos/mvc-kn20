<?php

namespace MVC\Controllers;

class Error extends Controller
{
    public function error404()
    {
        $this->view->setEnablePartials(false);
        $this->view->render('404');
    }
}