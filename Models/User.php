<?php

namespace MVC\Models;

class User extends AbstractUser
{
    private const TABLE = 'users';
    public string $country;
    protected string $status;
    protected string $createdAt;
    protected string $updatedAt;
    private array $fields = ["id", "email", "password", "status"];
    protected array $statuses = ["not active", "active", "disabled"];
    protected string $type = "user";

    public function __construct(
        int $id,
        string $firstName,
        string $lastName,
        string $email,
        string $password,
        string $country,
        string $status = 'not active',
        string $createdAt = null,
        string $updatedAt = null
    ) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->password = $password;
        $this->country = $country;
        if ($this->checkStatus($status)) {
            $this->status = $status;
        }
        $this->createdAt = $createdAt ?? date("Y-m-d H:i:s");
        $this->updatedAt = $updatedAt ?? date("Y-m-d H:i:s");
    }

    protected static function hashPassword(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    protected function checkStatus(string $status)
    {
        if (!in_array($status, $this->statuses)) {
            throw new Exception('Status is incorrect');
        }
        return true;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function __toString(): string
    {
        return "#" . $this->id . " " . $this->firstName . " " . $this->lastName;
    }

    public static function create(array $userData) : User
    {
        $stmt = Db::getInstance()->connect->prepare(
            "INSERT INTO `users` 
                ( 
                    `first_name`,
                    `last_name`,
                    `email`,
                    `password`,
                    `country`
                 )
            VALUES
                (
                    :first_name,
                    :last_name,
                    :email,
                    :password,
                    :country
                )"
        );
        $password = self::hashPassword($userData['password']);
        $stmt->execute(
            [
                "first_name" => $userData['firstName'],
                "last_name" => $userData['lastName'],
                "email" => $userData['email'],
                "password" => $password,
                "country" => $userData['country']
            ]
        );
        $id = Db::getInstance()->connect->lastInsertId();
        $user = new User(
            $id,
            $userData['firstName'],
            $userData['lastName'],
            $userData['email'],
            $password,
            $userData['country']
        );
        return $user;
    }

    public static function login(string $email, string $password) : User|bool
    {
        $stmt = Db::getInstance()->connect->prepare(
            "SELECT * FROM `users` WHERE `email` = :email"
        );
        $stmt->execute(array('email' => $email));
        $userData = $stmt->fetch();
        if (empty($userData)) {
            return false;
        }
        if (password_verify($password, $userData['password'])) {
            $user = new User(
                $userData['id'],
                $userData['first_name'],
                $userData['last_name'],
                $userData['email'],
                $userData['password'],
                $userData['country'],
                $userData['status'],
                $userData['create_at'],
                $userData['updated_at']
            );
            return $user;
        } else {
            return false;
        }
    }

    public function save(): void
    {
        $stmt = Db::getInstance()->connect->prepare(
            "UPDATE `users` 
            SET 
                `first_name` = :first_name,
                `last_name` = :last_name,
                `country` = :country,
                `status` = :status
            WHERE
                `id` = :id"
        );
        $stmt->execute(
            [
                'first_name' => $this->firstName,
                'last_name' => $this->lastName,
                'country' => $this->country,
                'status' => $this->status,
                'id' => $this->id
            ]
        );
    }
}