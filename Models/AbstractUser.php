<?php

namespace MVC\Models;

abstract class AbstractUser
{
    protected int $id;
    public string $firstName;
    public string $lastName;
    public string $email;
    protected string $password;

    abstract protected static function hashPassword(string $password): string;
}