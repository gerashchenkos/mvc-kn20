<?php

namespace MVC\Models;

use MVC\Models\Traits\Singleton;

class Db
{
    use Singleton;
    public \PDO $connect;

    protected function __construct()
    {
        $this->connect = new \PDO('mysql:host=localhost;dbname='. $_ENV['DB_NAME'], $_ENV['DB_USER'], $_ENV['DB_USER_PASS']);
    }
}