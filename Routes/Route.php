<?php

namespace MVC\Routes;

class Route
{
    private static $routes = [];
    private static $controllerNamspace = 'MVC\Controllers\\';

    public static function setRoutes(array $routes): void
    {
        self::$routes = $routes;
    }

    public static function init(string $url)
    {
        $controller = '';
        $action = '';
        foreach (self::$routes as $route) {
            if ($route['url'] === $url) {
                $controller = explode("@", $route["controller"])[0];
                $action = explode("@", $route["controller"])[1];
                break;
            }
        }
        if (empty($controller)) {
            $class = self::$controllerNamspace . "Error";
            $controller = new $class();
            $controller->error404();
            return;
        }
        $class = self::$controllerNamspace . $controller;
        $controller = new $class();
        $controller->{$action}();
    }

}